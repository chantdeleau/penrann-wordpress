<?php
/**
 * Thème pennrann
 * @author        Benoît Vigouroux
 * @link          www.watermelon-pixels.com
 *
 * En savoir plus : http://codex.wordpress.org/Template_Hierarchy
 *
 * @package       WordPress
 * @subpackage    pennrann
 * @since         pennrann 1.0
 *
 */ ?><!DOCTYPE html>
<!--[if IE 8 ]><html <?php language_attributes(); ?> class="ie8 no-js"><![endif]-->
<!--[if gte IE 9]><!--><html <?php language_attributes(); ?> class="no-js" prefix="og: http://ogp.me/ns#"><!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title><?php wp_title( '-', true, 'right' ); ?></title>
    <base href="<?php echo esc_url( home_url() ); ?>">
    <!-- La meta viewport la plus adaptée actuellement // @see : http://blog.goetter.fr/post/64389260648/windows-phone-8-0-orientation-et-initial-scale -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="pingback" href="<?php esc_url( bloginfo( 'pingback_url' ) ); ?>" />
    <!--Favicônes -->
      <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
      <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
      <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
      <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
      <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
      <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
      <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
      <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
      <link rel="icon" type="image/png" href="/favicon-196x196.png" sizes="196x196">
      <link rel="icon" type="image/png" href="/favicon-160x160.png" sizes="160x160">
      <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
      <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
      <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
      <meta name="msapplication-TileColor" content="#da532c">
      <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <!-- / Fin des favicônes -->
    <!-- Métas Facebook simples -->
      <meta property="og:title" content="<?php esc_attr( wp_title( '-', true, 'right' ) ); ?>" />
      <meta property="og:site_name" content="<?php esc_attr( bloginfo( 'name' ) ); ?>" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="<?php echo esc_url( get_permalink() ); ?>" />
    <!-- /Fin des métas Facebook simples -->
    <!-- Métas Twitter simples -->
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:url" content="<?php echo esc_url( get_permalink() ); ?>">
      <meta name="twitter:title" content="<?php esc_attr( wp_title( '-', true, 'right' ) );?>" />
      <?php if ( get_the_author_meta( 'twitter', 1 ) ) { ?>
      <meta name="twitter:creator" content="<?php esc_attr( the_author_meta( 'twitter', 1 ) ); ?>" />
      <?php } ?>
    <!-- /Fin des métas Twitter simples -->
    <?php if ( get_the_author_meta( 'google', 1 ) ) { ?>
    <!-- Authentification de l’auteur sur Google+ -->
    <link rel="author" href="<?php esc_url( the_author_meta('google', 1 ) ); ?>">
    <!-- / Fin de l’authentification de l’auteur sur Google+ -->
    <?php } ?>
    <link rel="alternate" type="application/rss+xml" title="<?php esc_attr( bloginfo( 'name' ) ); ?> | <?php _e( 'RSS feed', 'pennrann' ); ?>" href="<?php esc_url( bloginfo( 'rss2_url' ) ); ?>">
    <?php wp_head(); ?>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400,400i,700,700i,800,900" rel="stylesheet"> 
  </head>
  <body <?php body_class(); ?> role="document" itemscope itemtype="http://schema.org/WebPage">
    <div class="main-header">
      <div class="container">
          <nav class="skip-to flex" aria-hidden="true">
            <ul class="mw--site center mb0 mt0 p-reset small print-hidden" id="top">
              <li class="inbl m-reset">
                <a class="skip" href="<?php echo get_permalink(); ?>#nav"><?php _e( 'Skip to navigation', 'pennrann' ); ?></a>
              </li>
              <li class="inbl m-reset">
                <a class="skip" href="<?php echo get_permalink(); ?>#content"><?php _e( 'Skip to content', 'pennrann' ); ?></a>
              </li>
              <?php if ( is_singular( 'post' ) && function_exists( 'pennrann__sommaire' ) ) { ?>
              <li class="inbl m-reset">
                <a class="skip" href="<?php echo get_permalink(); ?>#toc"><?php _e( 'Skip to table of content', 'pennrann' ); ?></a>
              </li>
              <?php } ?>
            </ul>
          </nav>
        <header class="flex print-hidden" role="banner">
          <?php if ( is_front_page() ) : ?>
          <h1 itemprop="name" class="logo">
            <a href="<?php echo esc_url( home_url() ); ?>" itemprop="url" rel="home">
              <img alt="<?php bloginfo( 'name' ); ?>" class="header-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png">   
            </a>
          </h1>
          <?php else : ?>
          <p itemprop="name" class="logo">
            <a href="<?php echo esc_url( home_url() ); ?>" itemprop="url" rel="home">
              <img alt="<?php bloginfo( 'name' ); ?>" class="header-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png">   
            </a>
          </p>
          <?php endif; ?>
          <h2 class="visually-hidden" itemprop="description"><?php bloginfo( 'description' ); ?></h2>
          <button class="js-toggle-offcanvas offcanvas-button button btn-hamburger"></button>
        </header><!-- / banner -->
      </div><!-- / container -->
  </div><!-- / main-header -->
  <nav class="nav" id="offcanvas">
      <ul>
          <li><a href="#">Home</a></li>
          <li><a href="#">About</a></li>
          <li><a href="#">Portfolio</a></li>
          <li><a href="#">Projects</a></li>
          <li><a href="#">Contact</a></li>
      </ul>
  </nav>
  <nav class="clear print-hidden navigation" id="nav" role="navigation" aria-labelledby="nav-title">
    <div class="container">
      <h3 class="visually-hidden" id="nav-title"><?php _e( 'Main navigation', 'pennrann' ); ?></h3>
      <?php if ( has_nav_menu( 'primary' ) ) {
        wp_nav_menu(
          array(
            'theme_location' => 'primary',
            'menu_class'     => 'menu flex flex--auto',
            'items_wrap'     => '<ul class="%2$s">%3$s</ul>',
            'container'      => false
          )
        );
      } else {
        wp_dropdown_pages( array( 'depth' => 1 ) );
      } ?>
    </div><!-- / container -->
  </nav><!-- / #nav -->

<div id="main" class="panel">
  <?php if ( is_front_page() ) : ?>
    <!-- Slider main container -->
    <section class="home-slider-wrapper">
      <ul>
        <!-- Slides -->
        <li class="slide-item">
          <img src="https://unsplash.it/1920/500/?image=957" alt="" />
          <div class="slider-caption">
            <h3>Slide 1</h3>
          </div>
        </li>
        <li class="slide-item">
          <img src="https://unsplash.it/1920/500/?image=958" alt="" />
          <div class="slider-caption">
            <h3>Slide 2</h3>
          </div>
        </li>
        <li class="slide-item">
          <img src="https://unsplash.it/1920/500/?image=959" alt="" />
          <div class="slider-caption">
            <h3>Slide 3</h3>
          </div>
        </li>
      </ul>
    </section>
  <?php endif; ?>

  <div class="container">
    <main role="main" itemprop="mainContentOfPage">
      <div class="flex">
        <?php if ( function_exists('pennrann__ariane') && !is_front_page() ) {
          pennrann__ariane();
        } ?>
      </div>