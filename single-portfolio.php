<?php
/**
 * Template for displaying a portfolio post
 *
 * @package Portfolio Press
 */

get_header(); ?>

<section class="page__section flex">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" role="article" itemscope itemtype="http://schema.org/Article" <?php post_class( 'col col--12 col__md--9 col--first' ); ?>>
		<header class="entry-header">
			<h1 class="entry-title" itemprop="name"><?php the_title(); ?></h1>
			<div class="entry-meta">
				<time datetime="<?php the_time( 'Y-m-d' ); ?>" itemprop="datePublished"><?php the_time( __( 'j F Y', 'pennrann' ) ); ?></time>
			</div><!-- .entry-meta -->
		</header><!-- .entry-header -->
		<div class="entry-content itemprop="articleBody">
			<?php the_post_thumbnail(); ?>
			<?php the_content(); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'pennrann' ), 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->

		<?php pennrann__meta(); ?>

	</article><!-- #post-<?php the_ID(); ?> -->

	<?php if ( comments_open() ) {
		comments_template( '', true );
    } ?>

<?php endwhile; // end of the loop. ?>

    <?php get_sidebar(); ?>
</section>
<?php get_footer(); ?>