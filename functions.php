<?php
/**
 * pennrann : fonctions du thème - communes à l’administration et au front-end.
 * @author      Benoît Vigouroux
 * @link        www.watermelon-pixels.com
 *
 * @package     WordPress
 * @subpackage  pennrann
 * @since       pennrann 1.0
 */

/* ----------------------------- */
/* Sommaire */
/* ----------------------------- */
/*
  == Options du thème
  == Traduction
  == Colonnes latérales
  == Personnaliser le logo
  == Fonctions conditionnelles
    -- Fonctions de l’administration
    -- Fonctions du front-end
*/

  /* == @section Options du thème ==================== */
  /**
   * @see Twentytwelve - Thème WordPress par défaut.
   * @see http://wordpress.org/extend/themes/twentytwelve
  */
  add_theme_support( 'post-thumbnails' );
  add_theme_support( 'automatic-feed-links' );
  register_nav_menus( array( 'primary' => __( 'Main navigation', 'pennrann' ), 'lost' => __( 'Lost menu', 'pennrann' ) ) );
  if ( ! isset( $content_width ) ) { $content_width = 900; }


  /* == @section Traduction ==================== */
  /**
   * @author Luc Poupard
   * @see https://twitter.com/klohFR
   * @note I18n : déclare le domaine et l’emplacement des fichiers de traduction
   * @see Twentytwelve - Thème WordPress par défaut.
   * @link http://wordpress.org/extend/themes/twentytwelve
  */
  add_action( 'after_setup_theme', 'pennrann__setup' );
  function pennrann__setup() {
    load_theme_textdomain( 'pennrann', get_template_directory() . '/lang' );
  }


  /* == @section Colonnes latérales ==================== */
  /**
    @author Benoît Vigouroux
    @see https://twitter.com/ffoodd_fr
  */
  if ( ! function_exists( 'pennrann_widgets_init' ) ) {
    function pennrann_widgets_init() {

      // Une colonne latérale spécifique pour la page d’accueil
      register_sidebar( array(
        'name' => __( 'Home', 'pennrann' ),
        'id' => 'accueil',  
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget--title">',
        'after_title' => '</h3>',
      ) );

      // La colonne latérale pour les pages
      register_sidebar( array(
        'name' => __( 'Pages', 'pennrann' ),
        'id' => 'pages',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget--title">',
        'after_title' => '</h3>',
      ) );

      // 1ère Colonne du Footer
      register_sidebar( array(
        'name' => __( '1ère Colonne du Footer', 'pennrann' ),
        'id' => 'footer-first-col',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget--title">',
        'after_title' => '</h3>',
      ) );

      // 2nde Colonne du Footer
      register_sidebar( array(
        'name' => __( '2nde Colonne du Footer', 'pennrann' ),
        'id' => 'footer-second-col',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget--title">',
        'after_title' => '</h3>',
      ) );

      // 3ème Colonne du Footer
      register_sidebar( array(
        'name' => __( '3ème Colonne du Footer', 'pennrann' ),
        'id' => 'footer-third-col',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget--title">',
        'after_title' => '</h3>',
      ) );

      // 4ème Colonne du Footer
      register_sidebar( array(
        'name' => __( '4ème Colonne du Footer', 'pennrann' ),
        'id' => 'footer-fourth-col',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget--title">',
        'after_title' => '</h3>',
      ) );      
    }
  }
  add_action( 'widgets_init', 'pennrann_widgets_init' );


  /* == @section Personnaliser le logo ==================== */
  /**
   * @note Ajoute le support de la personnalisation de l’entête,
   * @note On le détourne pour personnaliser le logo.
   * @author Benoît Vigouroux

   */

  if ( ! function_exists( 'pennrann__logo' ) ) {
    function pennrann__logo() {
      $header_args = array(
        'default-image'       => get_template_directory_uri() . '/img/logo.png',
        'width'               => 180,
        'height'              => 180,
        'flex-width'          => true,
        'flex-height'         => true,
        'random-default'      => false,
        'header-text'         => false,
        'default-text-color'  => '',
        'uploads'             => true,
      );
      add_theme_support( 'custom-header', $header_args );
    }
  }
  add_action( 'after_setup_theme', 'pennrann__logo' );


  /* == @section Fonctions conditionnelles ==================== */
  /**
    @author Grégory Viguier
    @see https://twitter.com/ScreenFeedFr
    @see http://www.screenfeed.fr/blog/accelerer-wordpress-en-divisant-le-fichier-functions-php-0548/
    @note Version simple librement adaptée pour pennrann
    @author Benoît Vigouroux
    @see https://twitter.com/ffoodd_fr
  */

  /* -- @subsection Fonctions de l’administration -------------------- */
  if ( is_admin() ) {
    get_template_part ( 'inc/pennrann__functions', '-admin' );
  }

  /* -- @subsection Fonctions du front-end -------------------- */
  if ( !is_admin() ) {
    get_template_part ( 'inc/pennrann__functions', '-front' );
  }


  // INCLUDES
  require_once get_template_directory() . '/inc/activation.php';


  /**
   * Implement the Custom Header feature.
   */
  //require get_template_directory() . '/inc/custom-header.php';

  /**
   * Custom template tags for this theme.
   */
  //require get_template_directory() . '/inc/template-tags.php';

  /**
   * Custom functions that act independently of the theme templates.
   */
  //require get_template_directory() . '/inc/extras.php';

  /**
   * Customizer additions.
   */
  //require get_template_directory() . '/inc/customizer.php';

  /**
   * Load Jetpack compatibility file.
   */