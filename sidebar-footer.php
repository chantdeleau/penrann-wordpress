<?php
/**
 * La colonne latérale de la page d’accueil
 * @author        Benoît Vigouroux
 * @link          www.watermelon-pixels.com
 *
 * En savoir plus : http://codex.wordpress.org/Template_Hierarchy
 *
 * @package       WordPress
 * @subpackage    pennrann
 * @since         pennrann 1.0
 */

// On vérifie si la colonne latérale est active; si elle ne l’est pas, on ne l’affiche pas.
if ( ! is_active_sidebar( 'footer-first-col' ) ) {
  return;
} ?>

<aside class="col col--12  col__md--6 col__lg--3" role="complementary">
  <?php if ( is_active_sidebar( 'footer-first-col' ) ) { dynamic_sidebar( 'footer-first-col' ); } ?>
</aside>
<aside class="col col--12  col__md--6 col__lg--3" role="complementary">
  <?php if ( is_active_sidebar( 'footer-second-col' ) ) { dynamic_sidebar( 'footer-second-col' ); } ?>
</aside>
<aside class="col col--12  col__md--6 col__lg--3" role="complementary">
  <?php if ( is_active_sidebar( 'footer-third-col' ) ) { dynamic_sidebar( 'footer-third-col' ); } ?>
</aside>
<aside class="col col--12  col__md--6 col__lg--3" role="complementary">
  <?php if ( is_active_sidebar( 'footer-fourth-col' ) ) { dynamic_sidebar( 'footer-fourth-col' ); } ?>
</aside>