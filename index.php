<?php
/**
 * Fichier principal du thème
 * @author        Benoît Vigouroux
 * @link          www.watermelon-pixels.com
 *
 * En savoir plus : http://codex.wordpress.org/Template_Hierarchy
 *
 * @package       WordPress
 * @subpackage    pennrann
 * @since         pennrann 1.0
 */
get_header(); ?>

<?php if ( have_posts() ) { ?>

  <h2><?php _e( 'Recent posts', 'pennrann' ); ?></h2>

  <ol>
    <?php while ( have_posts() ) { the_post(); ?>
    <li <?php post_class( 'mb2' ); ?>>
      <article itemscope itemtype="http://schema.org/Article">
        <h3 itemprop="name">
          <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark" itemprop="url" tabindex="-1"><?php the_title(); ?></a>
        </h3>
        <p class="print-hidden" itemprop="UserComments"><?php comments_number( '0', '1', '% ' ); ?></p>
        <?php if ( has_post_thumbnail() ) { ?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" tabindex="-1" aria-hidden="true">
          <?php the_post_thumbnail( 'thumbnail', array( 'itemprop' => 'image', 'alt' => __( 'Permalink to the post', 'pennrann' ) ) ); ?>
        </a>
        <?php } ?>
        <time datetime="<?php the_time( 'Y-m-j' ); ?>" itemprop="datePublished"><?php the_time( __( 'j F Y', 'pennrann' ) ); ?></time>
        <?php $excerpt = get_the_excerpt() ?>
        <p itemprop="description"><?php echo $excerpt ?></p>
        <footer><?php pennrann__meta(); ?></footer>
      </article>
    </li>
    <?php } ?>
  </ol>

  <?php pennrann__pagination(); ?>

  <?php } else { ?>
    <h2><?php _e( 'Nothing found.', 'pennrann' ); ?></h2>
  <?php } ?>

<?php get_footer(); ?>