<?php
/**
 * Article
 * @author        Benoît Vigouroux
 * @link          www.watermelon-pixels.com
 *
 * En savoir plus : http://codex.wordpress.org/Template_Hierarchy
 *
 * @package       WordPress
 * @subpackage    pennrann
 * @since         pennrann 1.0
 * @see           http://schema.org/Article
 * @see           http://php.net/manual/fr/function.date.php
 */
get_header(); ?>

<section class="article__section flex">
  <?php if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>
    <article role="article" itemscope itemtype="http://schema.org/Article" <?php post_class( 'col col--12 col__md--9 col--first' ); ?>>
      <h1 itemprop="name"><?php the_title(); ?></h1>
      <time datetime="<?php the_time( 'Y-m-d' ); ?>" itemprop="datePublished"><?php the_time( __( 'j F Y', 'pennrann' ) ); ?></time>
      <div itemprop="articleBody">
        <?php the_content(); ?>
      </div>
      <footer>
        <?php if ( function_exists( 'pennrann__notes' ) ) {
          pennrann__notes();
        } ?>

        <?php pennrann__meta(); ?>

        <?php if ( function_exists( 'pennrann__partage' ) ) {
          pennrann__partage();
        } ?>
      </footer>
      <?php comments_template( '', true ); ?>
    </article>
  <?php get_sidebar();
    }
  } ?>
</section>
<?php get_footer(); ?>