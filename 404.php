<?php
/**
 * Page 404
 * @author        Benoît Vigouroux
 * @link          www.watermelon-pixels.com
 *
 * En savoir plus : http://codex.wordpress.org/Template_Hierarchy
 *
 * @package       WordPress
 * @subpackage    pennrann
 * @since         pennrann 1.0
 */
get_header(); ?>

  <article>
    <header>
      <h2><?php _e( '404', 'pennrann' ); ?></h2>
      <h3><?php _e( 'Page not found', 'pennrann' ); ?></h3>
    </header>
    <p><?php _e( 'Don\'t panic, we can help you. See:', 'pennrann' ); ?></p>
    <p class="h4-like"><?php _e( 'Here you are:', 'pennrann' ); ?> <span>&#10799;</span></p>
    <p><?php _e( 'If you\'re still lost, these suggestions may help:', 'pennrann' ); ?></p>
    <ul>
      <?php if ( has_nav_menu( 'lost' ) ) {
        wp_nav_menu(
          array(
            'theme_location' => 'lost',
            'items_wrap' => '%3$s',
            'container' => false
          )
        );
      } ?>
      <li>
        <a href="mailto:<?php echo antispambot( bloginfo( 'admin_email' ) ); ?>"><?php _e( 'Email this website\'s author', 'pennrann' ); ?></a>
      </li>
    </ul>
  </article>

<?php get_footer(); ?>
