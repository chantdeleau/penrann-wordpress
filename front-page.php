<?php
/**
 * Template Name: Accueil
 * Page d'accueil
 * @author        Benoît Vigouroux
 * @link          www.watermelon-pixels.com
 *
 * En savoir plus : http://codex.wordpress.org/Template_Hierarchy
 *
 * @package       WordPress
 * @subpackage    pennrann
 * @since         pennrann 1.0
 */
get_header(); ?>

<div class="flex">
	<?php if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>
	  <article class="col col--12" role="article" itemscope itemtype="http://schema.org/Article">
	    <h2 itemprop="name"><?php the_title(); ?></h2>
	    <div itemprop="articleBody"><?php the_content(); ?></div>
	  </article>
	<?php }
	  } ?>
</div><!-- / flex -->

<div class="flex home-main__col">
	<?php get_sidebar( 'accueil' ); ?>
</div><!-- / flex -->

<?php get_footer(); ?>