<?php
/**
 * Formulaire de recherche
 * @author Benoît Vigouroux
 * @link www.watermelon-pixels.com
 *
 * @package   WordPress
 * @subpackage  pennrann
 * @since     pennrann 1.0
 */
?>

  <form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label for="s" aria-hidden="true"><?php _e( 'Search', 'pennrann' ); ?></label>
    <input id="s" type="search" name="s" placeholder="<?php esc_attr_e( 'Search', 'pennrann' ); ?>" />
    <input type="submit" name="submit" value="<?php esc_attr_e( 'Search', 'pennrann' ); ?>" />
  </form>
