/* Tester l’activation du js
 * @author Benoît Vigouroux
 * @note Inspiré par Modernizr
 * @author http://modernizr.com/
 * @see http://modernizr.github.io/Modernizr/annotatedsource.html#section-103
*/
document.documentElement.className=document.documentElement.className.replace(/\bno-js\b/g,'')+' js';

/* Correction du bug des ancres sous Chrome
 * @see http://www.nczonline.net/blog/2013/01/15/fixing-skip-to-content-links/
 * @see http://blog.atalan.fr/des-liens-devitement-astucieux/
*/
window.addEventListener('hashchange', function(event) {
    var element = document.getElementById(location.hash.substring(1));
    if (element) {
        if (!/^(?:a|select|input|button)$/i.test(element.tagName)) {
            element.tabIndex = -1;
        }
        element.focus();
    }
}, false);


/* Liens d’évitements > Persistance de l’affichage
 * @see http://blog.atalan.fr/des-liens-devitement-astucieux/
*/
[].forEach.call(document.querySelectorAll('.skip'), function(el) {
  el.addEventListener('focus', function() {
    el.classList.add('show');
  });
});


$(document).ready(function() {
  /* Adding/Removing Class on Hover
  * https://css-tricks.com/snippets/jquery/addingremoving-class-on-hover/
  */

  $('.has-dropdown').hover(function () {
    $(this).toggleClass('hover');
    $('.sub-menu').toggleClass('hover');
  });


$('.has-dropdown').on({
    mouseenter : function() {
        $(this).addClass("hover");
    },
    mouseleave : function() {
        $(this).removeClass("hover");
    },

    mouseenter : function() {
        $('.sub-menu').addClass("hover");
    },
    mouseleave : function() {
        $('.sub-menu').removeClass("hover");
    },
});


  /* jQuery.offcanvas
  * A touch slideout navigation menu for your mobile web apps.
  * https://lgraubner.github.io/jquery-offcanvas/demo/advanced.html
  */
  var $el = $(".nav").offcanvas({
      effect: "slide-in-over",
      overlay: true,
      origin: "left"
  });

  $(".js-toggle-offcanvas").on("click.offcanvas", function() {
      $el.offcanvas("show");
  });


  /* Unslider
  * http://unslider.com/
  */
  $('.home-slider-wrapper').unslider({
    infinite: true,
    autoplay: true
  });

});