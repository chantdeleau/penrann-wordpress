				</main><!-- / #content -->
			</div><!-- / container -->
			<footer class="main-footer" role="contentinfo">
				<?php
				  include dirname(__FILE__) . '/tpl/socials-links.php';
				?>
				<div class="container">
					<div class="flex footer__col">
						<?php get_sidebar( 'footer' ); ?>
					</div>
				</div><!-- / flex -->
				<div class="flex footer__copyright">
					<div class="container flex--center">
						<small>&copy; <?php echo date( 'Y' ); ?> <?php bloginfo( 'name' ); ?>. <?php _e( 'By default, this content is licensed under', 'pennrann' ); ?> <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/fr/" class="footer__link" target="blank">Licence <abbr lang="en" title="Creative Commons">CC</abbr> <abbr lang="en" title="Attribution">BY</abbr>-<abbr lang="en" title="Non Commercial">NC</abbr>-<abbr lang="en" title="Share Alike">SA</abbr></a>.</small>
					</div>
				</div>
			</div>
		</footer><!-- / footer -->
		<!-- / push -->
		<?php wp_footer(); ?>
	</body>
</html>