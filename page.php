<?php
/**
 * Page standard
 * @author        Benoît Vigouroux
 * @link          www.watermelon-pixels.com
 *
 * En savoir plus : http://codex.wordpress.org/Template_Hierarchy
 *
 * @package       WordPress
 * @subpackage    pennrann
 * @since         pennrann 1.0
 */
get_header(); ?>

<section class="page__section flex">
	<?php if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>
	  <article class="col col--12 col__md--9 col--first" role="article" itemscope itemtype="http://schema.org/Article">
	    <h2 itemprop="name"><?php the_title(); ?></h2>
	    <div itemprop="articleBody"><?php the_content(); ?></div>
	  </article>
	<?php }
	} ?>
	<?php get_sidebar(); ?>
</section>

<?php get_footer(); ?>