<?php
/**
 * La colonne latérale des pages
 * @author        Benoît Vigouroux
 * @link          www.watermelon-pixels.com
 *
 * En savoir plus : http://codex.wordpress.org/Template_Hierarchy
 *
 * @package       WordPress
 * @subpackage    pennrann
 * @since         pennrann 1.0
 */

// On vérifie si la colonne latérale est active; si elle ne l’est pas, on ne l’affiche pas.
if ( ! is_active_sidebar( 'pages' ) && ! function_exists( 'pennrann__sommaire' ) ) {
  return;
} ?>

<aside class="col col--12 col__md--3 print-hidden" role="complementary">
  <?php if ( function_exists( 'pennrann__sommaire' ) && is_single() ) {
    echo do_shortcode( '[sommaire]' );
  } ?>
  <?php if ( is_active_sidebar( 'pages' ) ) { dynamic_sidebar( 'pages' ); } ?>
</aside>
