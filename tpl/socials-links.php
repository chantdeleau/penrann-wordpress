<?php
  include dirname(__FILE__) . '/../z-protect.php';
?>
<div class="reseaux-sociaux">
  <div class="container">
    <ul class="reseaux-sociaux_inner flex flex--auto">
      <li class="col">
        <a href="https://www.linkedin.com/company/effi'connect" target="_blank">
          <img class="alignnone size-full wp-image-34" src="http://www.efficonnect.fr/wp-content/uploads/logo-linkedin.png" alt="Efficonnect sur Linkedin" width="26" height="24">Restez connecté<br>
          sur Linkedin
        </a>
      </li>
      <li class="col">
        <a href="https://twitter.com/efficonnect" target="_blank">
          <img class="alignnone size-full wp-image-35" src="http://www.efficonnect.fr/wp-content/uploads/logo-twitter.png" alt="Efficonnect sur Twitter" width="24" height="20">Suivez-nous<br>
          sur Twitter
      </a>
      </li>
      <li class="col">
        <a href="http://www.viadeo.com/fr/company/effi-connect" target="_blank">
          <img class="alignnone size-full wp-image-36" src="http://www.efficonnect.fr/wp-content/uploads/logo-viadeo.png" alt="logo-viadeo" width="22" height="24">Suivez-nous<br>
          sur Viadeo
        </a>
      </li>
      <li class="col">
        <a href="/etre-rappele/">
          <img class="alignnone size-full wp-image-37" src="http://www.efficonnect.fr/wp-content/uploads/icone-contact.png" alt="icone-contact" width="25" height="17">Je souhaite<br>
          être contacté
      </a>
      </li>
      <li class="col">
        <a href="/contact/">
          <img class="alignnone size-full wp-image-33" src="http://www.efficonnect.fr/wp-content/uploads/icone-telephone.png" alt="icone-telephone" width="25" height="25">Contactez-nous<br>
          02 40 76 64 34
        </a>
      </li>
    </ul>
  </div><!-- container -->
</div><!-- reseaux-sociaux -->