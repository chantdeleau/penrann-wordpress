<?php
/**
 * La colonne latérale de la page d’accueil
 * @author        Benoît Vigouroux
 * @link          www.watermelon-pixels.com
 *
 * En savoir plus : http://codex.wordpress.org/Template_Hierarchy
 *
 * @package       WordPress
 * @subpackage    pennrann
 * @since         pennrann 1.0
 */

// On vérifie si la colonne latérale est active; si elle ne l’est pas, on ne l’affiche pas.
if ( ! is_active_sidebar( 'accueil' ) ) {
  return;
} ?>

<aside class="col col--3" role="complementary">
  <?php if ( is_active_sidebar( 'accueil' ) ) { dynamic_sidebar( 'accueil' ); } ?>
</aside>