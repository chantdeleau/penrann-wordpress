<?php

/**
 * Configuration after Theme activation
 *
 * @package default
 */

add_action('after_switch_theme', 'wputh_setup_theme');
function wputh_setup_theme() {

    // Options
    $wputh_setup_options = array(
        'date_format' => 'j F Y',
        'permalink_structure' => '/%postname%/',
        'timezone_string' => 'Europe/Paris',
        'time_format' => 'H:i',

        // Default values to avoid unnecessary queries
        'widget_calendar' => '',
        'widget_nav_menu' => '',
        'widget_pages' => '',
        'widget_post_categories' => '',
        'widget_tag_cloud' => '',
        'wpu_home_meta_description' => ''
    );

    $wputh_setup_options = apply_filters('wputh_setup_options', $wputh_setup_options);

    // Setting options
    foreach ($wputh_setup_options as $name => $value) {
        update_option($name, $value);
    }

    $default_folder = dirname(__FILE__) . '/activation/';

    // Updating permalinks
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}

    // Creating pages
    if (isset($_GET['activated']) && is_admin()){
            $new_page_title     = 'Plan du site';
            $new_page_content   = 'Vous souhaitez connaître le contenu du site : cette page vous permettra de retrouver les rubriques et informations que vous recherchez.';
            $new_page_template  = 'page-sitemap.php'; //ex. template-custom.php. Leave blank if you don't want a custom page template.
            //don't change the code bellow, unless you know what you're doing
            $page_check = get_page_by_title($new_page_title);
            $new_page = array(
                    'post_type'     => 'page',
                    'post_title'    => $new_page_title,
                    'post_content'  => $new_page_content,
                    'post_status'   => 'publish',
                    'post_author'   => 1,
            );
            if(!isset($page_check->ID)){
                    $new_page_id = wp_insert_post($new_page);
                    if(!empty($new_page_template)){
                            update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
                    }
            }
    }