<?php
/**
 * pennrann : fonctions du thème - partie administration
 * @author      Benoît Vigouroux
 * @link        www.watermelon-pixels.com
 *
 * @package     WordPress
 * @subpackage  pennrann
 * @since       pennrann 1.0
 */

/* ----------------------------- */
/* Sommaire */
/* ----------------------------- */
/*
  == Référencement Social / SEO
    -- Création des blocs dans l’administration
    -- Ajout des champs utiles dans ces blocs
    -- Sauvegarder la valeur de ces champs
  == Profil utilisateur
  == Désactive le lien par défaut sur les images
*/


  /* == @section Référencement Social / SEO ==================== */
  /**
   * @note Inspiré par le thème Noviseo2012, permet d’ajouter un champ «Titre» et «Description» à la zone d’édition
   * @author Sylvain Fouillaud
   * @see https://twitter.com/noviseo
   * @see http://noviseo.fr/2012/11/theme-wordpress-referencement/
   * @note Modifications :
   * @author Benoît Vigouroux

   * @note Homogénéisation du code, meilleure intégration dans l’administration, ajout des métas DublinCore et réorganisation des métas par contenu.
   */

  // On teste d’abord si la fonction est surchargée ou si un plugin dédié existe
  if (
    ! function_exists( 'pennrann__metabox' ) &&
    ! class_exists( 'WPSEO_Frontend' ) &&
    ! class_exists( 'All_in_One_SEO_Pack' )
  ) {

    /* -- @subsection Création des blocs dans l’administration -------------------- */
    function pennrann__metabox() {
      add_meta_box( 'pennrann__metabox__seo', __( 'SEO', 'pennrann' ), 'pennrann__metabox__contenu', 'post', 'side', 'high' );
      add_meta_box( 'pennrann__metabox__seo', __( 'SEO', 'pennrann' ), 'pennrann__metabox__contenu', 'page', 'side', 'high' );
    }
    add_action( 'add_meta_boxes', 'pennrann__metabox' );

    /* -- @subsection Ajout des champs utiles dans ces blocs -------------------- */
    function pennrann__metabox__contenu( $post ) {
      $val_title = get_post_meta( $post->ID, '_pennrann__metabox__titre', true );
      $val_canonical = get_post_meta( $post->ID, '_pennrann__metabox__canonical', true );
      $val_description = get_post_meta( $post->ID, '_pennrann__metabox__description', true ); ?>
      <p><?php _e( 'Those datas are used in <meta> tags for SEO and SMO.', 'pennrann' ); ?>.</p>
      <p><strong><?php _e( 'Title', 'pennrann' ); ?></strong></p>
      <p>
        <label class="screen-reader-text" for="pennrann__metabox__titre"><?php _e( 'Title', 'pennrann' ); ?></label>
        <input id="pennrann__metabox__titre" name="pennrann__metabox__titre" type="text" style="width:100%;" value="<?php echo $val_title; ?>" />
      </p>
      <p><strong><?php _e( 'Description', 'pennrann' ); ?></strong></p>
      <p>
        <label class="screen-reader-text" for="pennrann__metabox__description"><?php _e( 'Description', 'pennrann' ); ?></label>
        <textarea id="pennrann__metabox__description" name="pennrann__metabox__description" style="width:100%; resize:vertical;"><?php echo $val_description; ?></textarea>
      </p>
      <p><strong><?php _e( 'Canonical URL', 'pennrann' ); ?></strong></p>
      <p>
        <label class="screen-reader-text" for="pennrann__metabox__canonical"><?php _e( 'Canonical URL', 'pennrann' ); ?></label>
        <input id="pennrann__metabox__canonical" name="pennrann__metabox__canonical" placeholder="http://" type="url" style="width:100%;" value="<?php echo $val_canonical; ?>" />
      </p>
    <?php }

    /* -- @subsection Sauvegarder la valeur de ces champs -------------------- */
    function pennrann__metabox__save( $post_ID ) {
      if ( isset( $_POST['pennrann__metabox__titre'] ) ) {
        update_post_meta( $post_ID, '_pennrann__metabox__titre', sanitize_text_field( $_POST['pennrann__metabox__titre'] ) );
      }
      if ( isset( $_POST['pennrann__metabox__description'] ) ) {
        update_post_meta( $post_ID, '_pennrann__metabox__description', sanitize_text_field( $_POST['pennrann__metabox__description'] ) );
      }
      if ( isset( $_POST['pennrann__metabox__canonical'] ) ) {
        update_post_meta( $post_ID, '_pennrann__metabox__canonical', esc_url( $_POST['pennrann__metabox__canonical'] ) );
      }
    }
    add_action( 'save_post', 'pennrann__metabox__save' );
  }


  /* == @section Profil utilisateur ==================== */
  /**
   * @note Ajoute un champ «Twitter» et «Google+» dans les profils utilisateur
   * @note Supprime les champs inutiles
   * @author Valentin Brandt
   * @see https://twitter.com/geekeriesfr
   * @see http://www.geekeries.fr/snippet/gerer-champs-contact-profil-utilisateur-wordpress/
   */
  add_filter( 'user_contactmethods', 'pennrann__user', 75, 1 );
  if ( ! function_exists( 'pennrann__user' ) ) {
    function pennrann__user() {
      $contact['twitter'] = 'Twitter';
      $contact['google'] = 'Google+';
      return $contact;
    }
  }

  /* == @section Désactive le lien par défaut sur les images ==================== */
  /**
   * @note Par défaut, WordPress ajoute un lien vers le fichier lui-même lors de l’ajout dans le WYSIWYG, ce qui est inutile.
   * @author Syed Balkhi
   * @see https://plus.google.com/101623299936375408403/
   * @see http://www.wpbeginner.com/wp-tutorials/automatically-remove-default-image-links-wordpress/?utm_source=buffer&utm_campaign=Buffer&utm_content=buffer840f2&utm_medium=twitterhttp://www.geekeries.fr/snippet/gerer-champs-contact-profil-utilisateur-wordpress/
   */
  function pennrann__images() {
    $image_set = get_option( 'image_default_link_type' );
    if( $image_set !== 'none' ) {
      update_option( 'image_default_link_type', 'none' );
    }
  }
  add_action( 'admin_init', 'pennrann__images', 10 );


// Supprimer toutes les fonctionnalités liées aux commentaires dans votre WP
// https://dfactory.eu/turn-off-disable-comments/
// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
  $post_types = get_post_types();
  foreach ($post_types as $post_type) {
    if(post_type_supports($post_type, 'comments')) {
      remove_post_type_support($post_type, 'comments');
      remove_post_type_support($post_type, 'trackbacks');
    }
  }
}
add_action('admin_init', 'df_disable_comments_post_types_support');

// Close comments on the front-end
function df_disable_comments_status() {
  return false;
}
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);

// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
  $comments = array();
  return $comments;
}
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);

// Remove comments page in menu
function df_disable_comments_admin_menu() {
  remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'df_disable_comments_admin_menu');

// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
  global $pagenow;
  if ($pagenow === 'edit-comments.php') {
    wp_redirect(admin_url()); exit;
  }
}
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');

// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
  remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'df_disable_comments_dashboard');

// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
  if (is_admin_bar_showing()) {
    remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
  }
}
add_action('init', 'df_disable_comments_admin_bar');