<h2>Siège social</h2>
<?php bloginfo('name') ?>
Adresse
29000 PLOUIGNEAU
FRANCE
Tél. 02 98 00 00 00

<p>contact@<?php bloginfo('name') ?>.fr
Forme juridique :
Numéro d’immatriculation : 000 000 000 - RCS de Brest
Directeur de publication : </p>
<h2>Agence web</h2>
Le site <?php bloginfo('name') ?> a été conçu et développé par :
APPALOOSA
BP 10
Z.I. de Kerbriand
29610 Plouigneau
Tél. 02 98 79 81 00
<a href="http://www.appaloosa.fr" target="_blank">www.appaloosa.fr</a>
<h2>Hébergement</h2>
Ce site est hébergé par la société WP Engine :
<a href="https://wpengine.com/" target="_blank">WP Engine</a>
Inc. 504 Lavaca
Suite 1000
Austin
TX 78701
USA.
<h2>Conditions générales d’utilisation</h2>
<?php bloginfo('name') ?> a ouvert ce site pour l’information personnelle de ses utilisateurs. Aucune exploitation commerciale même partielle des données qui y sont présentées ne pourra être effectuée sans l’accord préalable et écrit de <?php bloginfo('name') ?>. Cette page détaille les conditions légales s’appliquant à chaque visiteur de ce site. Le fait de le consulter implique l’acceptation et l’application des spécifications de ce document de manière inconditionnelle.
<h2>Propriété intellectuelles &amp; Copyright</h2>
L’accès au Site confère au Client un droit d’usage privé et non exclusif. Le présent Site et tous ses contenus, à savoir notamment les marques, textes, logotypes, dessins, images, sons, brevets, etc., sont la propriété de <?php bloginfo('name') ?> et sont protégés par les dispositions du Code de la Propriété Intellectuelle. Toute utilisation des contenus, dénominations et marques, à savoir leur reproduction, leur représentation ou, plus généralement, leur exploitation, en tout ou partie, par quelque moyen et sur quelque support que ce soit, sans l’accord préalable et écrit de <?php bloginfo('name') ?> est interdite et constitue un acte de contrefaçon, réprimé pénalement et civilement.

Il est cependant tout à fait possible d’utiliser certains éléments sous forme de citation, en indiquant la source de la citation, à savoir « <?php bloginfo('name') ?> », et un lien visible et actif de l’article ou la page concernés du Site.

Toute autre utilisation nécessite l’accord exprès et préalable de <?php bloginfo('name') ?>. Il est tout à fait possible d’utiliser des liens hypertextes vers le site, y compris des liens profonds. En tout état de cause, <?php bloginfo('name') ?> se réserve le droit d’interdire de tels liens même tacitement autorisé, à la suite d’un contrôle a posteriori, ceux-ci devront alors être retirés à première demande de <?php bloginfo('name') ?>.

a) L’intégralité du présent site, la présentation et chacun des éléments, y compris les logos, graphismes, photographies, plans, vidéos, sons … etc, apparaissant sur le site sont protégés par les législations françaises et internationales en vigueur relatives à la propriété intellectuelle, et sont la propriété exclusive de <?php bloginfo('name') ?>.

b) Aucune licence, ni aucun autre droit que celui de consulter le "site" n'est conféré à quiconque au regard des droits de propriété intellectuelle. La reproduction des documents du site est autorisée aux fins exclusives d'information pour un usage personnel et privé ; toute reproduction ou toute utilisation de copies réalisées à d'autres fins est expressément interdite.

c) Utilisation des documents : toute information, image, vidéo, campagne publicitaire…etc sont protégés comme indiqué précédemment. Ces réalisations sont grevées de droits des tiers (i.e. photographes, mannequins, illustrateurs, réalisateurs…) qui font l’objet de cessions de droits qui ont été négociées de gré à gré. A ce titre tous les droits de reproduction, de représentation et de communication publique y compris les documents téléchargeables représentant des réalisations (affiches, presse, films…) sont réservés dans ce cadre précis.

Aucune exploitation commerciale n’est autorisée et les clauses du présent paragraphe a) b) c) sont applicables. Le non-respect desdites clauses et de leurs interdictions constituent une contrefaçon pouvant engager la responsabilité civile ou pénale du contrefacteur.
<h2>Données personnelles</h2>
Conformément à la loi française du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, les informations à caractère nominative relatives aux visiteurs pourront faire l’objet d’un traitement automatisé.

Chaque visiteur du site qui fournit des informations personnelles à <?php bloginfo('name') ?> consent à <?php bloginfo('name') ?> l'intégralité des droits transférables relatifs à cette information et autorise <?php bloginfo('name') ?> à en faire usage. Les informations ainsi fournies par les visiteurs sous leur responsabilité seront considérées comme non confidentielles, et devront être exactes, licites et ne pas nuire aux intérêts des tiers. <?php bloginfo('name') ?> s’engage à ne pas communiquer les informations recueillies à des tiers pour une exploitation commerciale.

Les visiteurs ont à tout moment la possibilité d'exercer leurs droits d'accès, de modification, de rectification et de suppression des données qui les concernent (art. 34 de la loi " Informatique &amp; Libertés " du 6 janvier 1978).

Pour exercer ce droit, s'adresser :
<ul>
 	<li>par courrier à : <?php bloginfo('name') ?> à l'adresse précisée ci-dessus (paragraphe siège social)</li>
 	<li>en utilisant le <a href="<?php bloginfo('url'); ?>/contact/">formulaire de contact</a>.</li>
</ul>
<h2>Conditions d'utilisations</h2>
Certaines pages du site contiennent des documents PDF. Pour lire ces documents, vous avez besoin du logiciel Acrobat Reader. <a href="https://get.adobe.com/fr/reader/otherversions/" target="_blank">Télécharger Acrobat Reader</a>

Afin de profiter pleinement de toutes les fonctionnalités du site Internet <?php bloginfo('name') ?>, l’utilisation des navigateurs Microsoft Internet Explorer 11.0+ (PC) , Chrome 56+, ou Firefox 51.0+ (Mac et PC) est vivement conseillée (logiciels gratuits).

Certaines pages du site contiennent des animations flash, des vidéos et des fonctionnalités avancées. Pour les visualiser, vous avez besoin du plug-in <a href="https://get.adobe.com/fr/flashplayer/">Adobe Flash Player </a>10+ (gratuit) et d'activer javascript.
<h2>Responsabilités et exclusion de garantie</h2>
La société décline toute responsabilité quant au contenu des sites proposés en liens.

Quel que soit le type de lien établi à partir d’un site Internet extérieur, dit ”site liant”, vers le site <?php bloginfo('name') ?> cette dernière se réserve le droit de s’opposer à son établissement.

Ni <?php bloginfo('name') ?>, ni un tiers impliqué dans la création de ce site, ne peut être tenu pour responsable d’éventuels dommages, directs ou indirects, survenant à l’occasion de l’accès ou des difficultés d’accès à ce site, ou de l’utilisation qu’il pourrait en être fait, y compris les virus susceptibles de contaminer la configuration du matériel informatique de l’utilisateur.

Les informations sur ce site sont fournies « telles quelles » sans aucune garantie, explicite ou implicite, relative à l’utilisation de la présente publication.

Les informations sur ce site peuvent inclure des inexactitudes ou des erreurs. <?php bloginfo('name') ?> se réserve le droit d’apporter des modifications, des améliorations et des changements sur les produits et les programmes décrits sur son site à tout moment et sans le notifier.

a) <?php bloginfo('name') ?> s'est efforcé d'assurer l'exactitude de l'ensemble des informations fournies sur son site. Malgré le soin apporté dans le choix des informations placées sur ce site, il est possible que celui-ci contienne des erreurs, imprécisions ou omissions. La responsabilité contractuelle de <?php bloginfo('name') ?> ne pourra en aucun cas être engagée quant à d'éventuelles erreurs ponctuelles et inopinées pouvant survenir sur le site. D’autre part, les informations communiquées sur ce site peuvent être modifiées et mises à jour sans préavis.

b) <?php bloginfo('name') ?> ne pourra être tenu pour responsable de tout dommage matériel ou immatériel, direct ou indirect, quelqu’en soient les causes (y compris ceux pouvant être causés par l’éventuelle diffusion de virus ou par la fraude informatique) ou conséquences résultant de la consultation de ce site, de son utilisation, des informations et de l’emploi des informations qu’il comporte ou encore de l’impossibilité d’y avoir accès, à l’exception des dommages directs consécutifs d’une faute lourde ou intentionnelle de <?php bloginfo('name') ?>.

c) De plus, les liens hypertextes placés sur ce site afin de compléter les informations ne sauraient engager la responsabilité de <?php bloginfo('name') ?> du fait de leur contenu; pas plus que le contenu des sites extérieurs désignant le site par un lien hypertexte, aucun contrôle ne pouvant être exercé par <?php bloginfo('name') ?> sur ces sites totalement indépendants. Tout utilisateur du site et des liens hypertextes doit se protéger des virus qui pourraient lui être inoculés lors de la visite du présent site ou des sites accessibles à partir du présent site par des liens hypertextes. <?php bloginfo('name') ?> ne garantit pas le fait que le serveur accueillant le site soit exempt de virus ou qu'il puisse survenir un problème technique qui pourrait endommager les composants de l'ordinateur de l'utilisateur ou des données qui pourraient y être stockées. En tout état de cause <?php bloginfo('name') ?> ou un de ses sous-traitants ne pourra être responsable d'un dommage quelconque pouvant se produire lors de la connexion sur le site.
<h2>Politique de cookies</h2>
<?php bloginfo('name') ?> a développé cette politique dans le but d’informer l’Utilisateur sur ce qu’est un cookie, quel type de cookies est utilisé sur ce site internet et de quelle manière l’Utilisateur peut les gérer selon ses intérêts. Lorsque l’Utilisateur visite ce site internet pour la première fois, il est informé de l’existence de cookies et de la présente “Politique de Cookies” de <?php bloginfo('name') ?>.

Lors de futures visites réalisées par l’Utilisateur sur ce Site Internet, celui-ci pourra consulter la présente Politique de Cookies à tout moment, dans l’onglet “Mentions légales” située sur la partie inférieure du Site Internet. En naviguant sur ce Site Internet, l’utilisateur consent que <?php bloginfo('name') ?> y installe dans son ordinateur les cookies décrits ci-dessous, sauf si l’Utilisateur a modifié la configuration de son navigateur pour refuser des cookies.

<strong>Qu’est-ce qu’un cookie ?</strong>
Les cookies sont des petits fichiers que certaines plateformes comme les sites internet, peuvent installer dans votre ordinateur. Leurs fonctions peuvent être très variées : ils permettent de stocker vos préférences de navigation, recueillir des informations statistiques, permettre certaines fonctionnalités techniques, etc. Parfois, les cookies sont utilisés pour stocker des informations de base sur les habitudes de navigation de l’Utilisateur ou de son ordinateur, jusqu’au point de pouvoir le reconnaître selon le cas. Les cookies sont aussi utilisés pour gérer la session de l’Utilisateur, en réduisant le nombre de fois où il est nécessaire d’introduire le mot de passe ou pour ajuster les contenus du site internet à ses préférences.

Les cookies peuvent être “temporaires”, c’est-à-dire qu’ils s’effacent de l’ordinateur de l’Utilisateur une fois que celui-ci abandonne le site internet qui les génère, ou “persistants”, c’est-à-dire des cookies qui restent dans l’ordinateur de l’Utilisateur jusqu’à une date déterminée. Pourquoi sont-ils importants ? Les cookies sont importants parce que très utiles pour les motifs suivants : d’un point de vue technique, ils permettent que les sites internet fonctionnent de manière plus fluide et adaptée aux préférences de l’Utilisateur (par exemple, les cookies stockent la langue de l’utilisateur ou la monnaie de son pays). En plus, ils aident les responsables des sites internet à améliorer leurs services grâce à l’information statistique recueillie par eux.

<strong>Comment l’utilisateur peut-il configurer les cookies ?</strong>
L’utilisateur peut permettre, bloquer ou éliminer les cookies installés dans son ordinateur au moyen de la configuration des options de son navigateur Internet.